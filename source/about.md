# ABOUT THIS KIT

### BACKGROUND

why and how this kit was conceived, the work and thinking behind it
connections with ETI's history and the changing scene of investigative work around the world (unless this duplicates with the above but it should have different aproach

### FOCUS
how and why we decided to focus this kit on evidence collection
### AUDIENCE

who does this kit address
### CONTENTS
what modules we included here and why these are relevant to start with

how are we are going about the kit: staged releases etc.


### ABOUT RIGHTS OF USE AND CONTRIBUTORS
