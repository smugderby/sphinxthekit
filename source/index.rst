
The Citizen Investigation Kit
=======================================

.. _preamble:

.. toctree::
  :maxdepth: 1
  :caption: Preamble

  preamble


.. _Introduction:

.. toctree::
   :maxdepth: 0
   :caption: Introduction

   about
   use

.. _INVESTIGATION-CONCEPTS:

.. toctree::
   :maxdepth: 1
   :caption: INVESTIGATION CONCEPTS

   investigation-concepts

.. _how-investigate:

.. toctree::
   :maxdepth: 2
   :caption: HOW YOU INVESTIGATE
   :glob:

   how/*

.. what-investigate:

.. toctree::
  :maxdepth: 1
  :caption: WHAT YOU INVESTIGATE
  :glob:

  what/*


.. _general:

.. toctree::
   :maxdepth: 1
   :caption: GENERAL CONSIDERATIONS
   :glob:

   general/*

.. _formats:

.. toctree::
   :maxdepth: 1
   :caption: FORMATS


    epub <https://sphinxthekit.netlify.com/epub/CitizenInvestigationKit.epub>
    pdf <https://sphinxthekit.netlify.com/latex/pdf/CitizenInvestigationKit.pdf>
